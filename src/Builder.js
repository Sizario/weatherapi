const Processor = require('./Processor');

module.exports = {
  setup: (type, initializationData) => {
    let forecast, serialize;

    switch (type) {

      case 'openweathermap':
        forecast = require('./apis/openweathermap/forecast')(initializationData);
        serialize = require('./apis/openweathermap/serialize')(initializationData);

        return new Processor(forecast, serialize);

      default:
        forecast = require('./apis/openweathermap/forecast')(initializationData);
        serialize = require('./apis/openweathermap/serialize')(initializationData);

        return new Processor(forecast, serialize);
    }
  }
};
