const createCsvWriter = require('csv-writer').createObjectCsvWriter;

class ReporterCSV {
  constructor(path) {
    this.path = path;

    this.csvWriter = createCsvWriter({
      path: `${path}/${new Date()}.csv`,
      header: [
        { id: 'date', title: 'Date' },
        { id: 'min', title: 'Minimum Temperature' },
        { id: 'max', title: 'Maximum Temperature' },
        { id: 'rain', title: 'Raining' }
      ]
    });
  }

  async writeReport(temperature, raining) {
    const records = [];

    try {
      Object.keys(temperature).forEach(date => {
        const temps = Object.keys(temperature[date]);

        records.push({
          date: date,
          min: temperature[date][Math.min(...temps)],
          max: temperature[date][Math.max(...temps)],
          rain: raining[date]
        });
      });

      this.csvWriter.writeRecords(records).then(() => {
        console.log('Report Created');
      });
    } catch (error) {
      console.log(error);
    }
  }
}

module.exports = ReporterCSV;
