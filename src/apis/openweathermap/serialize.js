
module.exports = ({ temperature, raining }) => {
  return async (forecast) => {
    forecast.list.forEach(data => {
      const time = data.dt;
      const temp = data.main.temp;

      if (!temperature[time]) temperature[time] = {};
      if (!temperature[time][temp]) temperature[time][temp] = [];

      temperature[time][temp].push(forecast.city.name);

      if (!raining[time]) raining[time] = [];
      if (data.rain !== undefined) {
        raining[time].push(forecast.city.name);
      }
    });
  }
};
