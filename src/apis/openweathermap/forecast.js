const fetch = require('node-fetch');

module.exports = ({ apiKey }) => {
  url = `http://api.openweathermap.org/data/2.5/forecast?APPID=${apiKey}`;

  return async (city) => {
    let error = null;
    let result = null;

    try {
      const response = await fetch(`${url}&q=${city}`, { method: 'GET' });

      if (response.status === 200) {
        result = await response.json();
      } else {
        error = await response.json();
      }
    } catch (internalError) {
      error = internalError;
    }

    return { error: error, result: result };
  }
};
