
class Processor {
  constructor(forecast, serialize) {
    this.forecast = forecast;
    this.serialize = serialize;
  }

  async process(cities) {
    let promises;

    try {
      promises = cities.map(city => this.forecast(city));

      await Promise.all(promises).then(async (forecasts) => {
        Promise.all(forecasts.map(forecast => {
          if (forecast.result) return this.serialize(forecast.result);

          console.log(forecast.error);
        }));
      });
    } catch (error) {
      console.log(error);
    }
  }
}

module.exports = Processor;
