# Weather API Task

### Environment
Project was developed on Linux environment (distribution: ubuntu 18.04)
Node.JS - v8.10.0
NPM - v3.5.2

### Dependencies
```
"csv-parser": "^2.3.2",
"csv-writer": "^1.5.0",
"node-fetch": "^2.6.0"
```

### Install
From root directory:
```
$ npm install
```

### Run
From root directory:
```
$ npm run start
```

### Authors
* **Evgeny Itkin**

### License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
