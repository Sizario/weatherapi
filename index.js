const Builder = require('./src/Builder');
const ReporterCSV = require('./src/utilities/ReporterCSV');

const reporter = new ReporterCSV('./reports');

async function main(cities) {
  const raining = {};
  const temperature = {};

  const processor = Builder.setup('openweathermap', {
    temperature, raining, apiKey: '{{YOUR_API_KEY}}'
  });

  await processor.process(cities);

  reporter.writeReport(temperature, raining);
}

const cities = [
  'Jerusalem', 'New York', 'Singapore', 'Seoul', 'Athens', 'Berlin', 'Paris', 'Oslo', 'Lisbon', 'Dubai'
];

setTimeout(main, 1000, cities);
